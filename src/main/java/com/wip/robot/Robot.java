/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.robot;

/**
 *
 * @author WIP
 */
public class Robot {
    int x, y, bx, by, N;
    char lastDirection = ' ';
    public Robot(int x, int y, int bx, int by, int N){
        this.N = N;
        if(!inMap(x, y)){
            this.x = 0;
            this.y = 0;
        }
        else{
            this.x = x;
            this.y = y;
        }
        if(!inMap(x, y)){
            this.bx = 0;
            this.by = 0;
        }
        else{
            this.bx = bx;
            this.by = by;
        }
    }
    public boolean inMap(int x, int y){
        if(x>=N || x<0 || y>=N || y<0){
            return false;
        }
        return true;
    }
    //N = y-1, S = y+1, E = x+1, W = x-1
    public boolean walk(char direction){
        if(direction=='N'){
            if(!inMap(x, y-1)){
                RobotMad();
                return false;
            }
            y = y - 1;
        }
        else if(direction=='S'){
            if(!inMap(x, y+1)){
                RobotMad();
                return false;
            }
            y = y + 1;
        }
        else if(direction=='E'){
            if(!inMap(x+1, y)){
                RobotMad();
                return false;
            }
            x = x + 1;
        }
        else if(direction=='W'){
            if(!inMap(x-1, y)){
                RobotMad();
                return false;
            }
            x = x - 1;
        }
        lastDirection = direction;
        if(isBomb()){
            found();
        }
        return true;
    }
    
    public boolean walk(char direction, int step){
        for(int i=0; i<step; i++){
            if(!Robot.this.walk(direction)){
                return false;
            }
        }
        return true;
    }
    
    public boolean walk(){
        Robot.this.walk(lastDirection);
        return true;
    }
    
    public boolean walk(int step){
        Robot.this.walk(lastDirection, step);
        return true;
    }
    
    public String toString(){
        return "Robot (" + this.x + "," + this.y +")" + "(" + Math.abs(x-bx) + "," + Math.abs(y-by) + ")";

    }
    public void RobotMad(){
        System.out.println("I can't move!!!");
    }
    public void found(){
        System.out.println("Bomb found!!!");
    }
    public boolean isBomb(){
        if(x == bx && y == by){
            return true;
        }
        return false;
    }
    
}
